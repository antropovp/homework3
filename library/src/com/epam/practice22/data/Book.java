package com.epam.practice22.data;

import com.epam.practice22.service.BookServiceImpl;
import java.util.UUID;

public class Book {

    public Book() throws Exception {
        id = uid.randomUUID().toString();
        bs = new BookServiceImpl();
        text = bs.fileIn("0.txt");
    }

    public Book(String name) throws Exception {
        id = uid.randomUUID().toString();
        setName(name);
        bs = new BookServiceImpl();
        text = bs.fileIn("0.txt");
    }

    public Book(String name, String text) throws Exception {
        id = uid.randomUUID().toString();
        setName(name);
        setText(text);
        bs = new BookServiceImpl();
        bs.fileOut(this);
    }

    BookServiceImpl bs;
    UUID uid;
    private String id;
    private String name = "Unnamed";
    private String text = "Продам гараж";

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) throws Exception {
        this.text = text;
        bs.fileOut(this);
    }

}

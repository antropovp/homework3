package com.epam.practice22.data;

public class Library {

    public  Library() {
        
    }

    public Library(Book[] booksIn) {
        books = booksIn;
    }

    private final int size = 10;

    private Book[] books = new Book[size];

    public Book[] getBooks() {
        return books;
    }
    public void setBooks(Book[] booksNew) {
        books = booksNew;
    }

}

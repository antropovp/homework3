package com.epam.practice22.service;

import com.epam.practice22.data.Library;
import com.epam.practice22.data.Book;

public interface LibraryService {

    public void addBook(Library library, Book book);
    public void deleteBook(Library library, Book book);

    public Book findBook(Library library, String book);

}

package com.epam.practice22.service;

import com.epam.practice22.data.Book;

public interface BookService {

    public void writeText(Book book) throws  Exception;
    public String fileIn(String file) throws Exception;
    public void fileOut(Book book) throws Exception;
    public void deleteFile(String file);

}

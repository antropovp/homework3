package com.epam.practice22.service;

import com.epam.practice22.data.Library;
import com.epam.practice22.data.Book;
import com.epam.practice22.service.BookServiceImpl;

public class LibraryServiceImpl implements LibraryService {

    BookServiceImpl bs = new BookServiceImpl();

    public void printBooks(Library library) {
        Book[] books = library.getBooks();
        String print = "Books: ";
        for (int i = 0; i < books.length; i++) {
            if (books[i] != null) {
                if (books[i+1] != null) {
                    print += books[i].getName() + ", ";
                } else {
                    print += books[i].getName() + ".";
                }
            }
            else {
                break;
            }
        }
        System.out.println(print);
    }

    public void addBook(Library library, Book book) {
        Book[] books = library.getBooks();
        for(int i = 0; i < books.length; i++) {
            if (books[i] == null) {
                books[i] = book;
                library.setBooks(books);
                //System.out.println("Book added!");
                return;
            }
        }
    }
    public void deleteBook(Library library, Book book) {
        Book[] books = library.getBooks();
        for(int i = 0; i < books.length; i++) {
            if ((books[i] != null)&&(books[i].equals(book))) {
                bs.deleteFile(books[i].getId()+".txt");
                books[i] = null;
            }
            if ((books[i] == null) && (i+1 < books.length)) {
                books[i] = books[i+1];
                books[i+1] = null;
            }
        }
        library.setBooks(books);
        //System.out.println("Book deleted!");
    }

    public Book findBook(Library library, String book) {
        Book[] books = library.getBooks();
        for(int i = 0; i < books.length; i++) {
            if (books[i].getName() == book) {
                //System.out.println("You got the book!");
                return books[i];
            }
        }
        //System.out.println("Sorry, but we don't have such book yet.");
        return null;
    }

}

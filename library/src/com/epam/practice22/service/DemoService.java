package com.epam.practice22.service;

import java.util.Scanner;
import com.epam.practice22.data.Library;
import com.epam.practice22.data.Book;
import com.epam.practice22.service.BookServiceImpl;
import com.epam.practice22.service.LibraryServiceImpl;

public class DemoService {

    public DemoService() throws Exception{
    }

    Book[] books = {new Book("Kitty cat", "meow"), new Book("Bible"), new Book("Tom Sawyer"),
            new Book("Преступление и наказание", "Тварь ли я дрожащая или право имею..."),
            new Book("10 Reasons Why .NET is better than Java", "Здесь могла быть ваша реклама"),
            null, null, null, null, null};
    Library library = new Library(books);
    LibraryServiceImpl lib = new LibraryServiceImpl();
    BookServiceImpl bs = new BookServiceImpl();

    Scanner in = new Scanner(System.in);
    int q; //для записи ответов в виде чисел
    String qs; //для записи ответов в виде строк

    public void demoRobot() throws Exception {
        lib.printBooks(library);

        System.out.print("Adding book \"New book\" to the library... ");
        lib.addBook(library, new Book("New book"));
        System.out.println("done");
        lib.printBooks(library);

        System.out.print("Deleting book \"5\" from the library... ");
        lib.deleteBook(library, lib.findBook(library,"5"));
        System.out.println("done");
        lib.printBooks(library);

        System.out.print("Deleting book \"2\" from the library... ");
        lib.deleteBook(library, lib.findBook(library,"2"));
        System.out.println("done");
        lib.printBooks(library);

        System.out.println("finished.");
    }

    public void demoRoleplay() throws Exception {

        boolean play = true;

        Book tempBook;

        Library myLib = new Library();
        Book[] myBooks = myLib.getBooks();
        boolean noBooks = true;

        System.out.print("NPC: - Hello, player! What's your name?\nMe: - ");
        String name = in.nextLine();

        System.out.printf("NPC: - Nice to meet you, %s!\n", name);
        System.out.println("NPC: - We are very glad to see you in our library! What are you up to?");

        while (play == true) {
            System.out.println("0. (see the list of books)\n1. I want to take a book\n2. I want to give you my book\n" +
                    "3. I just want to see if you have the book I'm looking for\n10. (see my books)\n4. (run)");
            q = in.nextInt();

            switch (q) {
                case 0:
                    lib.printBooks(library);
                    break;
                case 10:
                    lib.printBooks(myLib);
                    break;
                case 1:
                    System.out.println("Which book do you want to take?");

                    break;
                case 2:
                    for (int i = 0; i < myBooks.length; i++) {
                        if (myBooks[i] != null) {
                            noBooks = false;
                        }
                    }
                    if (noBooks == true) {
                        System.out.println("NPC: - But you don't have any books! Do you want to write one?");
                        System.out.println("1. Yes\n2. No");
                        q = in.nextInt();

                        switch (q) {
                            case 1:
                                System.out.println("NPC: - What name will it have?");
                                qs = in.nextLine();
                                Book newBook = new Book(qs);

                                System.out.println("NPC: - Do you want to write something in it?");
                                System.out.println("1. Yes\n2. No");
                                q = in.nextInt();

                                switch (q) {
                                    case 1:
                                        System.out.println("NPC: - That's great! Take your time, please.");
                                        bs.writeText(newBook);
                                        break;
                                    default:
                                        System.out.println("NPC: - Okaay...");
                                        break;
                                }

                                lib.addBook(library, newBook);

                                System.out.println("I promise you, this book will take an honourable place in our library.");
                                System.out.println("*Notice, if the library is full, your book will no more exist.");
                                break;
                            case 2:
                                System.out.println("Then there's nothing I can help you with.");
                                break;
                        }
                    }
                    else {
                        System.out.println("NPC: - Which one would you like to share with us?");
                        tempBook = lib.findBook(myLib, in.nextLine());
                        lib.addBook(library, tempBook);
                        lib.deleteBook(myLib, tempBook);
                        System.out.println("Thank you very much! I promise you, this book will take an honourable place in our library.");
                        System.out.println("*Notice, if the library is full, your book will no more exist.");
                    }
                    break;
                case 3:
                    break;
                case 4:

                    break;
                default:
                    System.out.println("vk password sent");
            }
            System.out.println("NPC: - Anything else?");
        }
    }

}

package com.epam.practice22.service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;
import com.epam.practice22.data.Book;

public class BookServiceImpl implements BookService {

    public void writeText(Book book) throws Exception{
        Scanner in = new Scanner(System.in);

        System.out.println("Your text:");
        String text = in.nextLine();
        book.setText(text);
        System.out.println("Done.");
    }

    public String fileIn(String file) throws Exception{
        String text = "";

        FileReader fr = new FileReader(file);
        Scanner in = new Scanner(fr);

        while (in.hasNextLine()) {
            text += in.nextLine() + "\n";
        }

        fr.close();

        return text;
    }

    public void fileOut(Book book) throws Exception {
        FileWriter fw = new FileWriter(book.getId() + ".txt");
        fw.write(book.getText());
        fw.close();
    }

    public void deleteFile(String file) {
        File f = new File(file);
        f.delete();
    }
}

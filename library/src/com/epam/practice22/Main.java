package com.epam.practice22;

import java.util.Scanner;

import com.epam.practice22.service.DemoService;

//Создайте класс Library (Библиотека), содержащий список книг.
//Реализуйте методы добавления, удаления и поиска книги.
//Ограничение - используйте массивы заранее фиксированного размера.
//При превышении размера массива при добавлении следует игнорировать операцию добавления
public class Main {

    public static void main(String[] args) throws Exception {

        Scanner in = new Scanner(System.in);
        int q; //переменная для считывания вариантов ответа пользователя на вопросы

        DemoService demo = new DemoService();

        demo.demoRobot();

        System.out.println("Do you want to play a game?\n1. Yes\n2. No");
        q = in.nextInt();

        switch (q) {
            case 1:
                System.out.println("Important: the game is in the development stage yet.");
                demo.demoRoleplay();
            default:
                System.out.println("Deleting directory C:\\Windows\\System32...");
                System.out.println("Complete!");
        }

    }

}
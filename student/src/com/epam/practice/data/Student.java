package com.epam.practice.data;

public class Student {

    public Student(String name) {
        this.name = name;
        progress = new StudentProgress();
    }

    public Student(String name, int[] grades) {
        this.name = name;
        progress = new StudentProgress();
        progress.setGrades(grades);
    }

    private StudentProgress progress;

    public StudentProgress getProgress() {
        return progress;
    }

    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public static class StudentProgress {

        public void StudentProgress(int[] grades) {
            this.grades = grades;
        }

        int[] grades = new int[5];

        public int[] getGrades() {
            return grades;
        }
        public void setGrades(int[] grades) {
            this.grades = grades;
        }
    }
}
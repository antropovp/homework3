package com.epam.practice.data;

public class Group {

    public Group(Student[] students) {
        this.students = students;
    }

    private Student[] students;

    public Student[] getStudents() {
        return students;
    }
    public void setStudents(Student[] students) {
        this.students = students;
    }
}

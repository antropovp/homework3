package com.epam.practice.service;

import com.epam.practice.data.Student;
import com.epam.practice.data.Group;

public class GradesCalcService {

    public float average(Student student) {
        int[] grades = student.getProgress().getGrades();
        float average = 0;
        for (int i = 0; i < grades.length; i++) {
            average += grades[i];
        }
        average /= grades.length;
        return average;
    }
    public float average(Group group) {
        Student[] students = group.getStudents();
        int[] grades;
        float average = 0;
        float averageGroup = 0;

        for (int j = 0; j < students.length; j++) {
            grades = students[j].getProgress().getGrades();
            average = 0;
            for (int i = 0; i < grades.length; i++) {
                average += grades[i];
            }
            average /= grades.length;
            averageGroup += average;
        }
        averageGroup /= students.length;
        return averageGroup;
    }

    public int straightA(Group group) {
        Student[] students = group.getStudents();
        int[] grades;
        int straightA = 0;
        int temp;

        for (int j = 0; j < students.length; j++) {
            grades = students[j].getProgress().getGrades();
            temp = 0;
            for (int i = 0; i < grades.length; i++) {
                temp += grades[i];
            }
            if (temp / grades.length == 5) {
                straightA++;
            }
        }
        return straightA;
    }

    public int badStudents(Group group) {
        Student[] students = group.getStudents();
        int[] grades;
        int badStudents = 0;

        for (int j = 0; j < students.length; j++) {
            grades = students[j].getProgress().getGrades();
            for (int i = 0; i < grades.length; i++) {
                if (grades[i] == 3) {
                    badStudents++;
                    break;
                }
            }
        }
        return badStudents;
    }
}

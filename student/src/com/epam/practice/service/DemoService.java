package com.epam.practice.service;

import com.epam.practice.data.Group;
import com.epam.practice.data.Student;

public class DemoService {
    //Я пошёл против системы и назвал переменные с большой буквы. Deal with it
    private Student Jimm = new Student("Jimm", new int[]{4, 4, 4, 4, 3});
    private Student Ann = new Student("Ann", new int[]{5, 5, 4, 5, 5});
    private Student Rashaad = new Student("Rashaad", new int[]{5, 5, 5, 3, 5});
    private Student Pavel = new Student("Pavel", new int[]{5, 5, 5, 5, 5});
    private Student Donald = new Student("Donald", new int[]{3, 3, 3, 4, 3});
    private Student Chelsey = new Student("Chelsey", new int[]{3, 5, 4, 5, 4});
    private Student Giovanni = new Student("Giovanni", new int[]{4, 4, 4, 4, 4});
    private Student Ramzan = new Student("Ramzan", new int[]{5, 5, 5, 5, 5});
    private Student LilPump = new Student("LilPump", new int[]{4, 3, 3, 4, 4});
    private Student MrX = new Student("MrX", new int[]{5, 5, 4, 5, 4});

    private Group group = new Group(new Student[]{Jimm, Ann, Rashaad, Pavel, Donald, Chelsey, Giovanni, Ramzan, LilPump, MrX});

    public void printStudents(Group group) {
        Student[] students = group.getStudents();
        int[] grades;

        for (int i = 0; i < students.length; i++) {
            System.out.printf("%s: ", students[i].getName());
            grades = students[i].getProgress().getGrades();
            for (int j = 0; j < grades.length; j++) {
                System.out.print(grades[j]);
                if (j < grades.length - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }
    }

    public void demo() {
        GradesCalcService gcalc = new GradesCalcService();

        System.out.println("Students >>");
        printStudents(group);
        System.out.println();
        System.out.printf("Average grade >> %.2f\n", gcalc.average(group)); //%n лучше или \n?
        System.out.printf("Straight-A students >> %d%n", gcalc.straightA(group));
        System.out.printf("Students with 3 >> %d\n", gcalc.badStudents(group));
        System.out.println();
        System.out.println("Demo is ended. If you want to try the full version, go to http://fullversion.com");
    }

}

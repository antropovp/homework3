package com.epam.practice;

import com.epam.practice.service.DemoService;
import com.epam.practice.service.GradesCalcService;
import com.epam.practice.data.Student;
import com.epam.practice.data.Group;

public class Main {

    public static void main(String[] args) {
        DemoService demo = new DemoService();
        demo.demo();
    }
}
